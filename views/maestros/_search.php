<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MaestrosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maestros-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_maestro') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'a_paterno') ?>

    <?= $form->field($model, 'a_materno') ?>

    <?= $form->field($model, 'rfc') ?>

    <?php // echo $form->field($model, 'fechaN') ?>

    <?php // echo $form->field($model, 'calle') ?>

    <?php // echo $form->field($model, 'colonia') ?>

    <?php // echo $form->field($model, 'numeroExt') ?>

    <?php // echo $form->field($model, 'correoElect') ?>

    <?php // echo $form->field($model, 'telefono') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
