<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaestrosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maestros';

?>
<div class="maestros-index">

    <h1><?= Html::encode($this->title) ?></h1>
     <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Crear Maestro', ['create'], ['class' => 'btn btn-success']) ?>
                </p> 
      <?php  } 
      ?>
    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_maestro',
            'nombre',
            'a_paterno',
            'a_materno',
            'rfc',
            //'fechaN',
            //'calle',
            //'colonia',
            //'numeroExt',
            //'correoElect',
            //'telefono',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
