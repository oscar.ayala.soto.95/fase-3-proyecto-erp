<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Maestros */

$this->title = $model->id_maestro;
$this->params['breadcrumbs'][] = ['label' => 'Maestros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="maestros-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_maestro], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_maestro], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas Seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_maestro',
            'nombre',
            'a_paterno',
            'a_materno',
            'rfc',
            'fechaN',
            'calle',
            'colonia',
            'numeroExt',
            'correoElect',
            'telefono',
        ],
    ]) ?>

</div>
