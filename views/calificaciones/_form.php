<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\dropDownList;
use app\models\Alumnos;
use app\models\Materias;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Calificaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calificaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_materia')->dropDownList(
        ArrayHelper::map(Materias::find()->all(), 'id_materia', 'nombre'),['prompt' => 
        'Seleccione una Materia',]
    ) ?>

<?= 
        
        //$model1 = Alumnos::find($model->id_alumno);
        //$name = $model1->nombre." ".$model->a_paterno;
        $form->field($model, 'id_alumno')->dropDownList(
        ArrayHelper::map(Alumnos::find()->all(), 'id_alumno', function($model) {
            return $model['nombre'].' '.$model['a_paterno']." ".$model['a_materno'];
        }),['prompt' => 
        'Seleccione un Alumno',]
    ) ?>
    <?= $form->field($model, 'Parcial1')->textInput() ?>

    <?= $form->field($model, 'Parcial2')->textInput() ?>

    <?= $form->field($model, 'Parcial3')->textInput() ?>

  

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
