<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CalificacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calificaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_calificacion') ?>

    <?= $form->field($model, 'id_materia') ?>

    <?= $form->field($model, 'id_alumno') ?>

    <?= $form->field($model, 'Parcial1') ?>

    <?= $form->field($model, 'Parcial2') ?>

    <?php // echo $form->field($model, 'Parcial3') ?>

    <?php // echo $form->field($model, 'Promedio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
