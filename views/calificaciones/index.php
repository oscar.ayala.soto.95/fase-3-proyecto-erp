<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalificacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calificaciones';

?>
<div class="calificaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Subir Calificaciones', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
    <?php  } 
    ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            [
                'attribute' => 'id_materia',
                'value' => 'materias.nombre',
            ],
            [
               
                'attribute' => 'id_alumno',
                'value' => function($model) { return $model->alumnos->nombre." ".$model->alumnos->a_paterno." ".$model->alumnos->a_materno   ;},
            ],
            
            'Parcial1',
            'Parcial2',
            'Parcial3',
            'Promedio',

            ['class' => 'yii\grid\ActionColumn'],
 
        ],
    ]); ?>


</div>
