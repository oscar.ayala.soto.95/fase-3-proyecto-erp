<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\dropDownList;
use app\models\Semestres;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Grupos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_semestre')->dropDownList(
        ArrayHelper::map(Semestres::find()->all(), 'id_semestre', 'nombre'),['prompt' => 
        'Seleccione un Semestre',]
    ) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
