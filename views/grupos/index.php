<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\GruposSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grupos';

?>
<div class="grupos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Crear un Grupo', ['create'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Semestres', ['/semestres/index'], ['class' => 'btn btn-warning']) ?>
                </p> 
    <?php  } else{
    ?>
               <p><?= Html::a('Semestres', ['/semestres/index'], ['class' => 'btn btn-warning']) ?> </p>
        <?php  }?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        
            [
                'attribute' => 'id_semestre',
                'value' => 'semestres.nombre',
            ],
            'descripcion',
 
            'cantidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
