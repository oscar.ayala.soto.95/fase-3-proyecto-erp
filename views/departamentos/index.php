<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DepartamentosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departamentos-index">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Crear Departamentos', ['create'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Campus', ['/campus/index'], ['class' => 'btn btn-info']) ?>
                </p> 
    <?php  } else{
    ?>
               <p><?= Html::a('Campus', ['/campus/index'], ['class' => 'btn btn-info']) ?> </p>
        <?php  }?>
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idDep',
            'nombreDep',
            'descripcion',
            [
                'attribute' => 'idCamp',
                'value' => 'campus.nombre',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
