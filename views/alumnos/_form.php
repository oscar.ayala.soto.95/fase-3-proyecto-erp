<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\dropDownList;
use app\models\Grupos;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumnos-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'id_grupo')->dropDownList(
        ArrayHelper::map(Grupos::find()->all(), 'id_grupo', 'descripcion'),['prompt' => 
        'Seleccione un Grupo',]
    ) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_paterno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_materno')->textInput(['maxlength' => true]) ?>

    
    <?= $form->field($model, 'fechaN')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         'language' => 'es',
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-MM-yyyy'
        ]
    ]);?>
    <?= $form->field($model, 'calle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'colonia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numeroExt')->textInput() ?>

    <?= $form->field($model, 'cp')->textInput() ?>

    <?= $form->field($model, 'telefono')->textInput() ?>

    <?= $form->field($model, 'correoElect')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
