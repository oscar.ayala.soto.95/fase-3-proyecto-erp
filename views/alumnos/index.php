<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlumnosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnos';

?>
<div class="alumnos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                  <?= Html::a('Crear Alumno', ['create'], ['class' => 'btn btn-success']) ?>
                  <?= Html::a('Calificaciones', ['calificaciones/index'], ['class' => 'btn btn-warning']) ?>
                </p>
    <?php  } else{ ?>
                <p>
                    <?= Html::a('Calificaciones', ['calificaciones/index'], ['class' => 'btn btn-warning']) ?>
                </p>
          <?php  } ?>
   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_alumno',
            [
                'attribute' => 'id_grupo',
                'value' => 'grupos.descripcion',
            ],
            'nombre',
            'a_paterno',
            'a_materno',
            //'fechaN',
            //'calle',
            //'colonia',
            //'numeroExt',
            //'cp',
            //'telefono',
            //'correoElect',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
