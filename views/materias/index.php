<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MateriasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Materias';

?>
<div class="materias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Crear Materia', ['create'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Materias-Alumnos', ['mostrar'], ['class' => 'btn btn-info']) ?>
                </p> 
    <?php  } else{
      ?>
            <p>
                <?= Html::a('Materias-Alumnos', ['mostrar'], ['class' => 'btn btn-info']) ?>
            </p>
    <?php } ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_materia',
            'nombre',
            'tipo',
            'descripcion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
