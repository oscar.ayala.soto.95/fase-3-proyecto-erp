<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalificacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Materias-Alumnos';

?>
<div class="calificaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

   
    
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_alumno' ,
            [
               
                'attribute' => 'nombre',
                'value' => function($model) { return $model->alumnos->nombre." ".$model->alumnos->a_paterno." ".$model->alumnos->a_materno   ;},
            ],
            'id_materia',
            [
                'attribute' => 'Materia',
                'value' => 'materias.nombre',
            ],

            'Promedio',

 
        ],
    ]); ?>


</div>
