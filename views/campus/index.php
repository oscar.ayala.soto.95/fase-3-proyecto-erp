<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CampusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campus-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Atras', ['/departamentos/index'], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('Crear Campus', ['create'], ['class' => 'btn btn-success']) ?>
                </p> 
    <?php  } else{
    ?>
        <p>
             <?= Html::a('Atras', ['/departamentos/index'], ['class' => 'btn btn-warning']) ?>
        </p>
    <?php   } ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idCamp',
            'nombre',
            'calle',
            'col',
            'numExt',
            //'cp',
            //'tel',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
