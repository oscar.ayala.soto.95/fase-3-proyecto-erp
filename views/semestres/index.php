<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\semestresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Semestres';

?>
<div class="semestres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                     <?= Html::a('Crear un Semestre', ['create'], ['class' => 'btn btn-success']) ?>
                </p> 
    <?php  } 
    ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_semestre',
            'nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
