<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Semestres */

$this->title = 'Actualizar Semestre: ' . $model->id_semestre;
$this->params['breadcrumbs'][] = ['label' => 'Semestres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_semestre, 'url' => ['view', 'id' => $model->id_semestre]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="semestres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
