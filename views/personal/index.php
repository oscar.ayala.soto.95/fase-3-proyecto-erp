<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personals';

?>
<div class="personal-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php  if ( yii::$app->user->identity->role == 2 )
            { ?>
                <p>
                    <?= Html::a('Crear Personal', ['create'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Departamentos', ['/departamentos/index'], ['class' => 'btn btn-danger']) ?>
                </p> 
    <?php  } else{
    ?>
               <p><?= Html::a('Departamentos', ['/departamentos/index'], ['class' => 'btn btn-danger']) ?> </p>
        <?php  }?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        
            'nombre',
            'aPaterno',
            'aMaterno',
            'fechNac',
            //'calle',
            //'col',
            //'numExt',
            //'cp',
            //'tel',
            //'rfc',
            [
                'attribute' => 'idDep',
                'value' => 'departamentos.nombreDep',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
