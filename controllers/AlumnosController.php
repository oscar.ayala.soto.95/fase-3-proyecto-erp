<?php

namespace app\controllers;

use Yii;
use app\models\Alumnos;
use app\models\AlumnosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Grupos;
use app\models\GruposSearch;

/**
 * AlumnosController implements the CRUD actions for Alumnos model.
 */
class AlumnosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alumnos models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        $searchModel = new AlumnosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alumnos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alumnos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = new Alumnos();

        if ($model->load(Yii::$app->request->post())) {
            $reg = Alumnos::find()->orderBy(['id_alumno' => SORT_DESC])->one();
            $model->id_alumno = $reg->id_alumno + 1;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_alumno]);
        }

        $searchModel = new GruposSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
        $this->layout = 'main3';
        return $this->render('create', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Alumnos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_alumno]);
        }
        $this->layout = 'main3';
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Alumnos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $this->findModel($id)->delete();
        $this->layout = 'main3';
        return $this->redirect(['index']);
    }

    /**
     * Finds the Alumnos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alumnos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alumnos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
