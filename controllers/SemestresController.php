<?php

namespace app\controllers;

use Yii;
use app\models\Semestres;
use app\models\semestresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SemestresController implements the CRUD actions for Semestres model.
 */
class SemestresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Semestres models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        $searchModel = new semestresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Semestres model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Semestres model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = new Semestres();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_semestre]);
        }
        $this->layout = 'main3';
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Semestres model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_semestre]);
        }
        $this->layout = 'main3';
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Semestres model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $this->findModel($id)->delete();
        $this->layout = 'main3';
        return $this->redirect(['index']);
    }

    /**
     * Finds the Semestres model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Semestres the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Semestres::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
