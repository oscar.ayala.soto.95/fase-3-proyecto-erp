<?php

namespace app\controllers;

use Yii;
use app\models\Calificaciones;
use app\models\CalificacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AlumnosSearch;
use app\models\MateriasSearch;

/**
 * CalificacionesController implements the CRUD actions for Calificaciones model.
 */
class CalificacionesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Calificaciones models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        $searchModel = new CalificacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Calificaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Calificaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = new Calificaciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->Parcial1 == 0 || $model->Parcial2 == 0 || $model->Parcial3 == 0){          
                $model->Promedio = 0;
                $model->save();      
            } 
            if($model->Parcial1 != 0 && $model->Parcial2 != 0 && $model->Parcial3 != 0){
                $total = $model->Parcial1 + $model->Parcial2 + $model->Parcial3;
                $model->Promedio = round(($total/3),1);
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id_calificacion]);
        }

        $searchModel = new AlumnosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 

        $searchModel2 = new MateriasSearch();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams); 

        $this->layout = 'main3';
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Calificaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->Parcial1 == 0 || $model->Parcial2 == 0 || $model->Parcial3 == 0){          
                $model->Promedio = 0;
                $model->save();      
            } 
            if($model->Parcial1 != 0 && $model->Parcial2 != 0 && $model->Parcial3 != 0){
                $total = $model->Parcial1 + $model->Parcial2 + $model->Parcial3;
                $model->Promedio =  round(($total/3),1);
                $model->save();
            }
            return $this->redirect(['index', 'id' => $model->id_calificacion]);
        }
        $this->layout = 'main3';
        return $this->render('update', [
            'model' => $model,
        ]);
    }

   

    /**
     * Deletes an existing Calificaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $this->findModel($id)->delete();
        $this->layout = 'main3';
        return $this->redirect(['index']);
    }

    /**
     * Finds the Calificaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calificaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calificaciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}



