<?php

namespace app\controllers;

use Yii;
use app\models\Materias;
use app\models;
use app\models\MateriasSearch;
use app\models\CalificacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MateriasController implements the CRUD actions for Materias model.
 */
class MateriasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Materias models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        $searchModel = new MateriasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Materias model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Materias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = new Materias();

        if ($model->load(Yii::$app->request->post()) ) {
             $reg = Materias::find()->orderBy(['id_materia' => SORT_DESC])->one();
             $model->id_materia = $reg->id_materia + 1;
             $model->save();
             
            return $this->redirect(['view', 'id' => $model->id_materia]);
        }
        $this->layout = 'main3';
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Materias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_materia]);
        }
        $this->layout = 'main3';
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionMostrar()
    {
        if ( yii::$app->user->identity->role == 1){
            $this->layout = 'main2';
        }
        if ( yii::$app->user->identity->role == 2){
            $this->layout = 'main3';
        }
        $searchModel = new CalificacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('mostrar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing Materias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if ( yii::$app->user->identity->role == 1 ){
            $this->layout = 'main2';
           return $this->render('/site/user');
        } 
        $this->findModel($id)->delete();

        $this->layout = 'main3';
        return $this->redirect(['index']);
    }

    /**
     * Finds the Materias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Materias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Materias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
