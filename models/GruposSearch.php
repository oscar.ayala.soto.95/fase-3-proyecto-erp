<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Grupos; use app\models\Semestres;

/**
 * GruposSearch represents the model behind the search form of `app\models\Grupos`.
 */
class GruposSearch extends Grupos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_grupo', 'cantidad'], 'integer'],
            [['descripcion', 'id_semestre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Grupos::find();
        $query->joinWith(['semestres']);
        

      

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        
           
        $query->andFilterWhere([
            'id_grupo' => $this->id_grupo,
           
            'cantidad' => $this->cantidad,
           
            
        ]);

        
        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);
        $query->andFilterWhere(['like', 'semestres.nombre', $this->id_semestre]);
        return $dataProvider;
    }
}
