<?php

namespace app\models;

use Yii;
use app\models;
/**
 * This is the model class for table "materia".
 *
 * @property int $id_materia
 * @property string $nombre
 * @property string $tipo
 * @property string $descripcion
 *
 * @property Calificacion[] $calificacions
 * @property Alumno[] $alumnos
 * @property MatAlumno[] $matAlumnos
 * @property Alumno[] $alumnos0
 */
class Materias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'nombre', 'tipo', 'descripcion'], 'required'],
            [['id_materia'], 'integer'],
            [['nombre', 'tipo', 'descripcion'], 'string', 'max' => 50],
            [['id_materia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_materia' => 'Id Materia',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Calificacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalificaciones()
    {
        return $this->hasMany(Calificaciones::className(), ['id_materia' => 'id_materia']);
    }

    
    /**
     * Gets query for [[Alumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::className(), ['id_alumno' => 'id_alumno'])->viaTable('calificacion', ['id_materia' => 'id_materia']);
    }

    /**
     * Gets query for [[MatAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatAlumnos()
    {
        return $this->hasMany(MatAlumno::className(), ['id_materia' => 'id_materia']);
    }

    /**
     * Gets query for [[Alumnos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos0()
    {
        return $this->hasMany(Alumno::className(), ['id_alumno' => 'id_alumno'])->viaTable('mat_alumno', ['id_materia' => 'id_materia']);
    }
}
