<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "departamentos".
 *
 * @property int $idDep
 * @property string $nombreDep
 * @property string $descripcion
 * @property int $idCamp
 *
 * @property Campus $idCamp0
 * @property Personal[] $personals
 */
class Departamentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departamentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idDep', 'nombreDep', 'descripcion', 'idCamp'], 'required'],
            [['idDep', 'idCamp'], 'integer'],
            [['nombreDep'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 100],
            [['idDep'], 'unique'],
            [['idCamp'], 'exist', 'skipOnError' => true, 'targetClass' => Campus::className(), 'targetAttribute' => ['idCamp' => 'idCamp']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idDep' => 'Id Departamento',
            'nombreDep' => 'Nombre Depart.',
            'descripcion' => 'Descripcion',
            'idCamp' => 'Id Campus',
        ];
    }

    /**
     * Gets query for [[IdCamp0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCamp0()
    {
        return $this->hasOne(Campus::className(), ['idCamp' => 'idCamp']);
    }
    public function getCampus()
    {
        return $this->hasOne(Campus::className(), ['idCamp' => 'idCamp']);
    }
    public function getCampus1(){
        return $this->hasOne(Campus::ClassName(), ['idDep' => 'idDep']);
    }
    /**
     * Gets query for [[Personals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonals()
    {
        return $this->hasMany(Personal::className(), ['idDep' => 'idDep']);
    }
}
