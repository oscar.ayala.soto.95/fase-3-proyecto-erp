<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campus".
 *
 * @property int $idCamp
 * @property string $nombre
 * @property string $calle
 * @property string $col
 * @property int $numExt
 * @property int $cp
 * @property int $tel
 *
 * @property Departamentos[] $departamentos
 */
class Campus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'calle', 'col', 'numExt', 'cp', 'tel'], 'required'],
            [['numExt', 'cp', 'tel'], 'integer'],
            [['nombre', 'calle', 'col'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCamp' => 'Id Campus',
            'nombre' => 'Nombre',
            'calle' => 'Calle',
            'col' => 'Colonia',
            'numExt' => 'Num. Ext.',
            'cp' => 'Codigo Postal',
            'tel' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Departamentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentos()
    {
        return $this->hasMany(Departamentos::className(), ['idCamp' => 'idCamp']);
    }
}
