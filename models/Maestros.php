<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maestro".
 *
 * @property int $id_maestro
 * @property string $nombre
 * @property string $a_paterno
 * @property string $a_materno
 * @property string $rfc
 * @property string $fechaN
 * @property string $calle
 * @property string $colonia
 * @property int $numeroExt
 * @property string $correoElect
 * @property int $telefono
 */
class Maestros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maestro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'a_paterno', 'a_materno', 'rfc', 'fechaN', 'calle', 'colonia', 'numeroExt', 'correoElect', 'telefono'], 'required'],
            [['fechaN'], 'safe'],
            [['numeroExt', 'telefono'], 'integer'],
            [['nombre', 'a_paterno', 'a_materno', 'rfc', 'calle', 'colonia', 'correoElect'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_maestro' => 'Id-Maestro',
            'nombre' => 'Nombre',
            'a_paterno' => 'Apellido Paterno',
            'a_materno' => 'Apellido Materno',
            'rfc' => 'RFC',
            'fechaN' => 'Fecha de Nacimiento',
            'calle' => 'Calle',
            'colonia' => 'Colonia',
            'numeroExt' => 'Numero Exterior',
            'correoElect' => 'Correo Electronico',
            'telefono' => 'Telefono',
        ];
    }
}
