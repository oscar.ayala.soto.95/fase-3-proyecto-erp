<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calificaciones;

/**
 * CalificacionesSearch represents the model behind the search form of `app\models\Calificaciones`.
 */
class CalificacionesSearch extends Calificaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_calificacion',  'Parcial1', 'Parcial2', 'Parcial3'], 'integer'],
            [['Promedio'], 'double'],
            [['id_materia', 'id_alumno'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calificaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_calificacion' => $this->id_calificacion,
            'id_materia' => $this->id_materia,
            'id_alumno' => $this->id_alumno,
            'Parcial1' => $this->Parcial1,
            'Parcial2' => $this->Parcial2,
            'Parcial3' => $this->Parcial3,
            'Promedio' => $this->Promedio,
        ]);

        return $dataProvider;
    }
}
