<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property int $id_personal
 * @property string $nombre
 * @property string $aPaterno
 * @property string $aMaterno
 * @property string $fechNac
 * @property string $calle
 * @property string $col
 * @property string $numExt
 * @property int $cp
 * @property int $tel
 * @property string $rfc
 * @property int $idDep
 *
 * @property Departamentos $idDep0
 */
class Personal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'aPaterno', 'aMaterno', 'fechNac', 'calle', 'col', 'numExt', 'cp', 'tel', 'rfc', 'idDep'], 'required'],
            [['cp', 'tel'], 'integer'],
            [['nombre', 'aPaterno', 'aMaterno', 'calle', 'col', 'idDep'], 'string', 'max' => 50],
            [['fechNac'], 'safe'],
            [['numExt'], 'string', 'max' => 6],
            [['rfc'], 'string', 'max' => 20],
            [['idDep'], 'exist', 'skipOnError' => true, 'targetClass' => Departamentos::className(), 'targetAttribute' => ['idDep' => 'idDep']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_personal' => 'Id Personal',
            'nombre' => 'Nombre',
            'aPaterno' => 'A. Paterno',
            'aMaterno' => 'A. Materno',
            'fechNac' => 'Fecha de Nacimiento',
            'calle' => 'Calle',
            'col' => 'Colonia',
            'numExt' => 'Num. Ext.',
            'cp' => 'Codigo Postal',
            'tel' => 'Telefono',
            'rfc' => 'RFC',
            'idDep' => 'Id Dep',
        ];
    }

    /**
     * Gets query for [[IdDep0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdDep0()
    {
        return $this->hasOne(Departamentos::className(), ['idDep' => 'idDep']);
    }
    public function getDepartamentos(){
        return $this->hasOne(Departamentos::ClassName(), ['idDep' => 'idDep']);
    }
}
