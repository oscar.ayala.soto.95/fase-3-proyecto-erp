<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumno".
 *
 * @property int $id_alumno
 * @property int $id_grupo
 * @property string $nombre
 * @property string $a_paterno
 * @property string $a_materno
 * @property string $fechaN
 * @property string $calle
 * @property string $colonia
 * @property int $numeroExt
 * @property int $cp
 * @property int $telefono
 * @property string $correoElect
 *
 * @property Grupos $grupo
 * @property Calificacion[] $calificacions
 * @property Materia[] $materias
 * @property MatAlumno[] $matAlumnos
 * @property Materia[] $materias0
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'id_grupo', 'nombre', 'a_paterno', 'a_materno', 'fechaN', 'calle', 'colonia', 'numeroExt', 'cp', 'telefono', 'correoElect'], 'required'],
            [['id_alumno',  'numeroExt', 'cp', 'telefono'], 'integer'],
            [['fechaN'], 'safe'],
            [['nombre','id_grupo', 'a_paterno', 'a_materno', 'calle', 'colonia'], 'string', 'max' => 50],
            [['correoElect'], 'string', 'max' => 100],
            
            [['id_alumno'], 'unique'],
            [['id_grupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::className(), 'targetAttribute' => ['id_grupo' => 'id_grupo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_alumno' => 'Id-Alumno',
            'id_grupo' => 'Id-Grupo',
            'nombre' => 'Nombre',
            'a_paterno' => 'Apellido Paterno',
            'a_materno' => 'Apellido Materno',
            'fechaN' => 'Fecha de Nacimiento',
            'calle' => 'Calle',
            'colonia' => 'Colonia',
            'numeroExt' => 'Numero Exterior',
            'cp' => 'Codigo Postal',
            'telefono' => 'Telefono',
            'correoElect' => 'Correo Electtronico',
        ];
    }

    /**
     * Gets query for [[Grupo]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getGrupos(){
        return $this->hasOne(Grupos::ClassName(), ['id_grupo' => 'id_grupo']);
    }
    public function getGrupo()
    {
        return $this->hasOne(Grupos::className(), ['id_grupo' => 'id_grupo']);
    }
    public function getFullname(){
        return $this->nombre.' '.$this->a_peterno;
    }
    /**
     * Gets query for [[Calificacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalificaciones()
    {
        return $this->hasOne(Calificaciones::className(), ['id_alumno' => 'id_alumno']);
    }

    /**
     * Gets query for [[Materias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterias()
    {
        return $this->hasMany(Materia::className(), ['id_materia' => 'id_materia'])->viaTable('calificacion', ['id_alumno' => 'id_alumno']);
    }

    /**
     * Gets query for [[MatAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatAlumnos()
    {
        return $this->hasOne(MatAlumno::className(), ['id_alumno' => 'id_alumno']);
    }

    /**
     * Gets query for [[Materias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterias0()
    {
        return $this->hasMany(Materia::className(), ['id_materia' => 'id_materia'])->viaTable('mat_alumno', ['id_alumno' => 'id_alumno']);
    }

    public function getMaterias1() {
        return $this->hasOne(Materias::className(), ['id_materia' => 'id_materia'])
            ->viaTable('calificacion', ['id_alumno' => 'id_alumno']);
    }
}
