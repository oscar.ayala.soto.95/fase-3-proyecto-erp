<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semestres".
 *
 * @property int $id_semestre
 * @property string $nombre
 *
 * @property Grupos[] $grupos
 */
class Semestres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semestres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_semestre' => 'Id Semestre',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Grupos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrupos()
    {
        return $this->hasMany(Grupos::className(), ['id_semestre' => 'id_semestre']);
    }
}
