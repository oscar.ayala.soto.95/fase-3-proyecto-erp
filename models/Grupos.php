<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grupos".
 *
 * @property int $id_grupo
 * @property int $id_semestre
 * @property string $descripcion
 * @property int $cantidad
 *
 * @property Alumno $alumno
 * @property Semestres $semestre
 */
class Grupos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_semestre', 'descripcion', 'cantidad'], 'required'],
            [[ 'cantidad'], 'integer'],
            [['descripcion', 'id_semestre',], 'string', 'max' => 100],
            [['id_semestre'], 'exist', 'skipOnError' => true, 'targetClass' => Semestres::className(), 'targetAttribute' => ['id_semestre' => 'id_semestre']],
        ];
    }

   
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_grupo' => 'Id Grupo',
            'id_semestre' => 'Id Semestre',
            'descripcion' => 'Descripcion',
            'cantidad' => 'Cantidad',
        ];
    }
    public function getSemestre(){
        return $this->hasOne(Semestres::ClassName(),['id_grupo','id_semestre']);
    }
    /**
     * Gets query for [[Alumno]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno()
    {
        return $this->hasOne(Alumno::className(), ['id_grupo' => 'id_grupo']);
    }

    /**
     * Gets query for [[Semestre]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemestres()
    {
        return $this->hasOne(Semestres::className(), ['id_semestre' => 'id_semestre']);
    }
}
