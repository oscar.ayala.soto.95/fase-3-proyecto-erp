<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Maestros;

/**
 * MaestrosSearch represents the model behind the search form of `app\models\Maestros`.
 */
class MaestrosSearch extends Maestros
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_maestro', 'numeroExt', 'telefono'], 'integer'],
            [['nombre', 'a_paterno', 'a_materno', 'rfc', 'fechaN', 'calle', 'colonia', 'correoElect'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Maestros::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_maestro' => $this->id_maestro,
            'fechaN' => $this->fechaN,
            'numeroExt' => $this->numeroExt,
            'telefono' => $this->telefono,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'a_paterno', $this->a_paterno])
            ->andFilterWhere(['like', 'a_materno', $this->a_materno])
            ->andFilterWhere(['like', 'rfc', $this->rfc])
            ->andFilterWhere(['like', 'calle', $this->calle])
            ->andFilterWhere(['like', 'colonia', $this->colonia])
            ->andFilterWhere(['like', 'correoElect', $this->correoElect]);

        return $dataProvider;
    }
}
