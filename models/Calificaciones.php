<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calificacion".
 *
 * @property int $id_calificacion
 * @property int $id_materia
 * @property int $id_alumno
 * @property int $Parcial1
 * @property int $Parcial2
 * @property int $Parcial3
 * @property float $Promedio
 *
 * @property Alumno $alumno
 * @property Materia $materia
 */
class Calificaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calificacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_materia', 'id_alumno', 'Parcial1', 'Parcial2', 'Parcial3'], 'required'],
            [['Parcial1', 'Parcial2', 'Parcial3'], 'integer'],
            [['Promedio'], 'double'],
            [['id_materia', 'id_alumno'], 'string', 'max' => 100],
            [['id_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['id_alumno' => 'id_alumno']],
            [['id_materia'], 'exist', 'skipOnError' => true, 'targetClass' => Materias::className(), 'targetAttribute' => ['id_materia' => 'id_materia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_calificacion' => 'Id Calificacion',
            'id_materia' => 'Id Materia',
            'id_alumno' => 'Id Alumno',
            'Parcial1' => 'Parcial 1',
            'Parcial2' => 'Parcial 2',
            'Parcial3' => 'Parcial 3',
            'Promedio' => 'Promedio',
        ];
    }

    /**
     * Gets query for [[Alumno]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getAlumnos(){
        return $this->hasOne(Alumnos::ClassName(), ['id_alumno' => 'id_alumno']);
    }
    public function getMaterias(){
        return $this->hasOne(Materias::ClassName(), ['id_materia' => 'id_materia']);
    }
    public function getAlumno()
    {
        return $this->hasOne(Alumno::className(), ['id_alumno' => 'id_alumno']);
    }

    /**
     * Gets query for [[Materia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMateria()
    {
        return $this->hasOne(Materia::className(), ['id_materia' => 'id_materia']);
    }
}
